package compiler.phases.lexan;

import java.io.*;
import common.report.*;
import compiler.phases.*;
import java.util.Random;

/**
 * Lexical analysis.
 * 
 * @author sliva
 *
 */
public class LexAn extends Phase {

	/** The name of the source file. */
	private final String srcFileName;

	/** The source file reader. */
	private final BufferedReader srcFile;

	/**
	 * Column index of the first (unread) character.
	 * The index is incremented when a character is read from source and is used 
	 * for determining the location of the currently parsed symbol.
	 * 
	 * The index is reset to zero when a newline is read from source - at the
	 * same time the row index is incremented.
	 */
	private int colIdx;
	private int rowIdx;
	
	/**
	 * A single character buffer for the character that terminated a successful
	 * match.
	 * It is used as the first character for the next match.
	 */
	char unconsumedChar;
	
	/** Table of matchers, to which we compare currently unmatched part of the
	 *  consumed source. */
	Matcher[] matchers;
	
	/* Helper function for leter/digit/printable checks. */
	private boolean isLetter(char c) {
		return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
	}
	private boolean isDigit(char c) {
		return c >= '0' && c <= '9';
	}
	private boolean isPrintable(char c) {
		return c >= ' ' && c <= '~';
	}
	private boolean isWhiteSpace(char c) {
		return c == ' ' || c == '\t' || c == '\n';
	}
	
	private char getChar() throws IOException {
		char consumed;
		if (srcFile.ready()) {
			consumed = (char)srcFile.read();
			colIdx++;
		}
		// we have reached end of file while still having possible matches
		// - we emit a NULL character which should terminate all matchers
		// (and possibly emit a symbol) - this is to handle the case when the
		// source file ends without a newline
		else
			consumed = 0;

		// if we get a carriage return (happens when you create a file in a 
		// MS Win environment) we assume that it always followed by a line feed
		// character -  we silently consume CR and handle the following LF
		if(consumed == '\r')
			consumed = getChar();
		// if we get a line feed character, we reset/increment column/row
		// counters - the character should still be handled by matchers
		if(consumed == '\n') {
			colIdx = 1;
			rowIdx++;
		}
		return consumed;
	}
	
	private void skipNonSymbols() throws IOException {
		char consumed = unconsumedChar;
		// we have to make sure that the unconsumed termination character
		// represents a valid symbol start in the next lexify call
		// therefore we skip any whitespace or comments
		while(isWhiteSpace(consumed))
			consumed = getChar();
		// a comment continues to the end of the current line - we also have to
		// ignore any leading whitespace in the next line
		// more consecutive comments are allowed
		while(consumed == '#') {
			while(srcFile.ready() && consumed != '\n')
				consumed = getChar();
			// skip any leading whitespace in the next line
			while(isWhiteSpace(consumed))
				consumed = getChar();
		}
		unconsumedChar = consumed;
	}
	
	/**
	 * Matcher interface.
	 * Checks if the provided consumed character matches the current state of the
	 * matcher.
	 * 
	 * Since their state could be modified with each call of match, it should
	 * be reset before we try to compare the whole definition again.
	 */
	private abstract class Matcher {
		Term definitionTerm;
		boolean matches;
		
		Matcher(Term definitionTerm) {
			this.definitionTerm = definitionTerm;
		}
		
		abstract boolean match(char consumed);
		/** Forces the matcher to match from the start of the definition. */
		void reset() {
			matches = true;
		}
		/* final method calling the overridable reset function */
		private void resetMatcher() {
			reset();
		}
		
		/** Returns if the symbol representation has explicit delimiters, which
				have to be removed before the symbol content is returned. */
		boolean hasExplicitDelimiters() {
			return false;
		}
		/** Checks if the symbol, when properly matched, is followed by a proper
		 *	termination character (currently consumed) - only then we can emit its
		 *  term. */
		abstract boolean validEnding(char consumed);
		
		/** Returns if the content of the currently unmatched consumed source is a
		 *	valid candidate for this matcher.
		 *	This means that the consumed content is valid for this matcher up to
		 *	the last checked character, but it might not conform to the definition
		 *	with the characters following it.
		 * 
		 *	This is needed for the greedy matching approach since we stop when
		 *	we have only one candidate and one term returned.
		 */
		boolean isCandidate() {
			return matches;
		}
		/** Returns if the matcher defines a keyword. */
		boolean definesKeyword() {
			return false;
		}
		/** Returns the term for the symbol definition of this matcher. */
		Term getDefinitionTerm() {
			return definitionTerm;
		}
	}
	/**
	 * Checks if the character is a part of the keyword/operator definition.
	 * That is, it matches a symbol with a defined length and content, with an
	 * implicit terminator.
	 * 
	 * It does a character-by-character comparison with the definition string
	 * (in a case sensitive manner), comparing the next character with each call.
	 * 
	 * Due to the greedy nature of lexer, we emit the term in the call after the
	 * full match, since we have to make sure that the keyword is not a part of
	 * an identifier (and also to make sure that some other definition with the
	 * same common root isn't valid at the same time - therefore all longer
	 * common root operators should be initialized in the lookup table before
	 * shorter ones).
	 */
	private abstract class FixedDefinitionMatcher extends Matcher {
		OperatorMatcher[] commonRootOperators;
		
		String definition;
		int curIdx;
		
		FixedDefinitionMatcher(Term definitionTerm, String definition,
						OperatorMatcher[] commonRootOperators) {
			super(definitionTerm);
			this.definition = definition;
			this.commonRootOperators = commonRootOperators;
		}
		FixedDefinitionMatcher(Term definitionTerm, String definition) {
			this(definitionTerm, definition, null);
		}
		@Override boolean isCandidate() {
			if(commonRootOperators != null) {
				for(OperatorMatcher commonRootOperator : commonRootOperators)
					if(commonRootOperator.isCandidate())
						return false;
			}
			return matches;
		}
		@Override boolean match(char consumed) {
			if(matches) {
				if(curIdx < definition.length())
					matches = consumed == definition.charAt(curIdx++);
				// after we have completely matched the definition, it should not be
				// followed with a character that could be a part of an identifier
				// - if we match a keyword, it should not be followed immediately by
				//   an alphanumeric character or _
				// - if we match an operator, it can be followed by any character
				else
					return isCandidate() && (matches = validEnding(consumed));
			}
			return false;
		}
		@Override void reset() {
			super.reset();
			curIdx = 0;
		}
	}
	/* specific keyword and operand implementations */
	private class KeywordMatcher extends FixedDefinitionMatcher {
		public KeywordMatcher(Term definitionTerm, String definition,
						OperatorMatcher[] commonRootOperators) {
			super(definitionTerm, definition, commonRootOperators);
		}
		public KeywordMatcher(Term definitionTerm, String definition) {
			super(definitionTerm, definition);
		}
		@Override boolean definesKeyword() {
			return true;
		}
		@Override boolean validEnding(char consumed) {
			return !(isLetter(consumed) || isDigit(consumed) || consumed == '_');
		}
	}
	private class OperatorMatcher extends FixedDefinitionMatcher {
		public OperatorMatcher(Term definitionTerm, String definition,
						OperatorMatcher[] commonRootOperators) {
			super(definitionTerm, definition, commonRootOperators);
		}
		public OperatorMatcher(Term definitionTerm, String definition) {
			super(definitionTerm, definition);
		}
		
		@Override boolean validEnding(char consumed) {
			// since no operator definition coincides with the definition of an
			// identifier, they are matched early even when using a greedy matching
			// approach
			// therefore, any character following a complete match can be a valid
			// ending for the currently matched operator, even if it is followed by
			// the same operator
			// (so ==== is matched as == ==)
			return true;
		}
	}
	/**
	 * Checks if the character is a part of a symbol, delimited by a start/stop
	 * rule or with an explicit terminator.
	 */
	private abstract class RuleMatcher extends Matcher {
		boolean firstChar;
		boolean lastChar;
		
		RuleMatcher(Term definitionTerm) {
			super(definitionTerm);
		}
		@Override boolean match(char consumed) {
			if(matches) {
				if(firstChar && !(firstChar = !firstChar))
					matches = validStart(consumed);
				// termination character immediately follows the last valid
				// content character
				else if(lastChar)
					return isCandidate();
				else if(lastChar = validEnding(consumed))
					return isCandidate() && !hasExplicitDelimiters();
				else
					matches = validContent(consumed) || validEnding(consumed);
			}
			return false;
		}
		@Override void reset() {
			super.reset();
			firstChar = true;
			lastChar = false;
		}
		/** Checks if the current character represents valid content of a symbol. */
		abstract boolean validContent(char consumed);
		/** Checks if the current character represent a valid starting character
				(that is, a valid starting delimiter) of a symbol. */
		abstract boolean validStart(char consumed);
	}
	private abstract class NondelimitedRuleMatcher extends RuleMatcher {
		public NondelimitedRuleMatcher(Term definitionTerm) {
			super(definitionTerm);
		}
		@Override boolean validEnding(char consumed) {
			return !validContent(consumed);
		}
	}
	private abstract class DelimitedRuleMatcher extends RuleMatcher {
		DelimitedRuleMatcher(Term definitionTerm) {
			super(definitionTerm);
		}
		@Override boolean validEnding(char consumed) {
			return validStart(consumed);
		}
		@Override boolean hasExplicitDelimiters() {
			return true;
		}
	}
	/* integer/character/identifier implementation */
	private class IntegerMatcher extends NondelimitedRuleMatcher {
		IntegerMatcher() {
			super(Term.INTCONST);
		}
		@Override boolean validContent(char consumed) {
			return isDigit(consumed);
		}
		@Override boolean validStart(char consumed) {
			return validContent(consumed);
		}
	}
	private class CharacterMatcher extends DelimitedRuleMatcher {
		// since it is a character (not a string) constant, we should only match
		// when we have only one character inside delimiters
		int consumedLength;
		@Override void reset() {
			super.reset();
			consumedLength = 0;
		}
		CharacterMatcher() {
			super(Term.CHARCONST);
		}
		@Override boolean match(char consumed) {
			if(matches)
				return (matches = consumedLength<=1) &&
								super.match(consumed) && (matches = consumedLength == 1);
			return false;
		}
		@Override boolean validContent(char consumed) {
			if(consumed != '\'' && isPrintable(consumed)) {
				consumedLength++;
				return true;
			}
			return false;
		}
		@Override boolean validStart(char consumed) {
			return consumed=='\'';
		}
	}
	private class IdentifierMatcher extends NondelimitedRuleMatcher {
		IdentifierMatcher() {
			super(Term.IDENTIFIER);
		}
		@Override boolean isCandidate() {
			for(Matcher matcher : matchers)
				if(matcher.definesKeyword() && matcher.isCandidate())
					return false;
			return matches;
		}
		@Override boolean validContent(char consumed) {
			return isLetter(consumed) || isDigit(consumed) || consumed == '_';
		}
		@Override boolean validStart(char consumed) {
			return isLetter(consumed);
		}
	}

	/**
	 * Constructs a new lexical analysis phase.
	 * @param skipFileInit to skip file initialization
	 */
	public LexAn(boolean skipFileInit) {
		super("lexan");
    if(skipFileInit) {
			srcFileName = null;
			srcFile = null;
		}
		else {
			srcFileName = compiler.Main.cmdLineArgValue("--src-file-name");
			try {
				srcFile = new BufferedReader(new FileReader(srcFileName));
				// we put the first char in the unconsumed buffer
				unconsumedChar = getChar();
				skipNonSymbols();
			} catch (IOException ___) {
				throw new Report.Error("Cannot open source file '" + srcFileName + "'.");
			}
		}
		colIdx = 1;
		rowIdx = 1;
		
		OperatorMatcher
						equOp,
						neqOp,
						leqOp,
						geqOp;
		
		// we initalize the matcher table
		matchers = new Matcher[]{
			// IOR
			new OperatorMatcher(Term.IOR, "|"),
			// XOR
			new OperatorMatcher(Term.XOR, "^"),
			// AND
			new OperatorMatcher(Term.AND, "&"),
			// EQU
			equOp = new OperatorMatcher(Term.EQU, "=="),
			// ASSIGN
			new OperatorMatcher(Term.ASSIGN, "=", new OperatorMatcher[]{equOp}),
			// NEQ
			neqOp = new OperatorMatcher(Term.NEQ, "!="),
			// NOT
			new OperatorMatcher(Term.NOT, "!", new OperatorMatcher[]{neqOp}),
			// LEQ
			leqOp = new OperatorMatcher(Term.LEQ, "<="),
			// LTH
			new OperatorMatcher(Term.LTH, "<", new OperatorMatcher[]{leqOp}),
			// GEQ
			geqOp = new OperatorMatcher(Term.GEQ, ">="),
			// GTH
			new OperatorMatcher(Term.GTH, ">", new OperatorMatcher[]{geqOp}),
			// ADD
			new OperatorMatcher(Term.ADD, "+"),
			// SUB
			new OperatorMatcher(Term.SUB, "-"),
			// MUL
			new OperatorMatcher(Term.MUL, "*"),
			// DIV
			new OperatorMatcher(Term.DIV, "/"),
			// MOD
			new OperatorMatcher(Term.MOD, "%"),
			// MEM
			new OperatorMatcher(Term.MEM, "$"),
			// VAL
			new OperatorMatcher(Term.VAL, "@"),
			// NEW
			new KeywordMatcher(Term.NEW, "new"),
			// DEL
			new KeywordMatcher(Term.DEL, "del"),
			// COLON
			new OperatorMatcher(Term.COLON, ":"),
			// COMMA
			new OperatorMatcher(Term.COMMA, ","),
			// DOT
			new OperatorMatcher(Term.DOT, "."),
			// SEMIC
			new OperatorMatcher(Term.SEMIC, ";"),
			// LBRACE
			new OperatorMatcher(Term.LBRACE, "{"),
			// RBRACE
			new OperatorMatcher(Term.RBRACE, "}"),
			// LBRACKET
			new OperatorMatcher(Term.RBRACKET, "["),
			// RBRACKET
			new OperatorMatcher(Term.RBRACKET, "]"),
			// LPARENTHESIS
			new OperatorMatcher(Term.LPARENTHESIS, "("),
			// RPARENTHESIS
			new OperatorMatcher(Term.RPARENTHESIS, ")"),
			// VOIDCONST
			new KeywordMatcher(Term.VOIDCONST, "none"),
			// BOOLCONST
			new KeywordMatcher(Term.BOOLCONST, "true"),
			new KeywordMatcher(Term.BOOLCONST, "false"),
			// CHARCONST
			new CharacterMatcher(),
			// INTCONST
			new IntegerMatcher(),
			// PTRCONST
			new KeywordMatcher(Term.PTRCONST, "null"),
			// VOID
			new KeywordMatcher(Term.VOID, "void"),
			// BOOL
			new KeywordMatcher(Term.BOOL, "bool"),
			// CHAR
			new KeywordMatcher(Term.CHAR, "char"),
			// INT
			new KeywordMatcher(Term.INT, "int"),
			// PTR
			new KeywordMatcher(Term.PTR, "ptr"),
			// ARR
			new KeywordMatcher(Term.ARR, "arr"),
			// REC
			new KeywordMatcher(Term.REC, "rec"),
			// DO
			new KeywordMatcher(Term.DO, "do"),
			// ELSE
			new KeywordMatcher(Term.ELSE, "else"),
			// END
			new KeywordMatcher(Term.END, "end"),
			// FUN
			new KeywordMatcher(Term.FUN, "fun"),
			// IF
			new KeywordMatcher(Term.IF, "if"),
			// THEN
			new KeywordMatcher(Term.THEN, "then"),
			// TYP
			new KeywordMatcher(Term.TYP, "typ"),
			// VAR
			new KeywordMatcher(Term.VAR, "var"),
			// WHERE
			new KeywordMatcher(Term.WHERE, "where"),
			// WHILE
			new KeywordMatcher(Term.WHILE, "while"),
			// IDENTIFIER
			new IdentifierMatcher(),
		};
	}
	public LexAn() {
		this(false);
	}
	
	/**
	 * The lexer.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called
	 * until it returns EOF. This method calls {@link #lexify()}, logs its
	 * result if requested, and returns it.
	 * 
	 * @return The next symbol from the source file.
	 */
	public Symbol lexer() {
		Symbol symb = lexify();
		symb.log(logger);
		return symb;
	}

	@Override
	public void close() {
		try {
			srcFile.close();
		} catch (IOException ___) {
			Report.warning("Cannot close source file '" + this.srcFileName + "'.");
		}
		super.close();
	}

	// --- LEXER ---
	/**
	 * Performs the lexical analysis of the source file.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called
	 * until it returns EOF.
	 * 
	 * @return The next symbol from the source file or EOF if no symbol is
	 *         available any more.
	 */
	private Symbol lexify() {
		StringBuilder symbolString = new StringBuilder();
		
		try {
			// we save the current (starting) row/column position
			int startColIdx = colIdx;
			int startRowIdx = rowIdx;
			// we also have to save the ending row/column position (which excludes
			// the termination character)
			int endColIdx = colIdx;
			int endRowIdx = rowIdx;
			
			// we reset all matchers
			for(Matcher matcher : matchers)
				matcher.reset();
			
			Matcher validMatcher = null;
			int possibleMatchCount = 0;
			
			char consumed = unconsumedChar;
			while(srcFile.ready() || unconsumedChar != 0) {
				// we go through all matchers, count all candidates and remember the
				// last non-null one
				validMatcher = null;
				possibleMatchCount = 0;
				
				for(Matcher matcher : matchers) {
					if(matcher.match(consumed)) {
						validMatcher = matcher;
						/*
						Report.info(new Location(startRowIdx, startColIdx, rowIdx, colIdx),
									"!  matched "+matcher.getDefinitionTerm().name()+
									" in \""+symbolString.toString()+"["+consumed+"]\"");
						*/
					}
					if(matcher.isCandidate()) {
						/*
						if(validMatcher != matcher)
							Report.info(new Location(startRowIdx, startColIdx, rowIdx, colIdx),
										"?  possible candidate for "+matcher.getDefinitionTerm().name()+
										" in \""+symbolString.toString()+consumed+"\"");
						*/
						possibleMatchCount++;
					}
				}
				// we get our match when there is only one candidate and when this
				// candidate emits its term (it might take multiple characters!) - at
				// this point, the last remembered term is also the correctly matched
				// one
				if(possibleMatchCount == 1 && validMatcher != null)
					break;
				// if there are no candidates for the current unmatched input, we throw
				// an error
				if(possibleMatchCount == 0)
					throw new Report.Error(
									new Location(startRowIdx, startColIdx, rowIdx, colIdx),
									"Could not match \""+symbolString.toString()+"["+consumed+"]"+
									"\" to any language construct.");
				// there are still possible matches and the currently consumed character
				// is part of them - we add it to the symbol string
				symbolString.append(consumed);
				// we save what might be the ending position
				endColIdx = colIdx;
				endRowIdx = rowIdx;
				// we put the last consumed (termination) character in the unmatched
				// buffer to be read in the next lexify call
				consumed = getChar();
			}
			unconsumedChar = consumed;
			skipNonSymbols();
			// if we matched a symbol definition, we emit its symbol
			if(validMatcher != null) {
				Location emittedSymbolLocation =
								new Location(startRowIdx, startColIdx, endRowIdx, endColIdx);
				String emittedSymbolString = 
								validMatcher.hasExplicitDelimiters() ?
								symbolString.toString().substring(1, symbolString.length()-1) :
								symbolString.toString();
				/*
				Report.info(emittedSymbolLocation,
								"=> matched term "+validMatcher.getDefinitionTerm().name()+
								" in symbol \""+emittedSymbolString+"\"");
				*/
				return new Symbol(validMatcher.getDefinitionTerm(), emittedSymbolString,
								new Location(startRowIdx, startColIdx, endRowIdx, endColIdx));
			}
			// if we reached the end of file without a single match, we emit an EOF
			return new Symbol(Term.EOF, "", new Location(rowIdx, colIdx));
		}
		catch (IOException e) {
			throw new Report.InternalError();
		}
	}
	
	private boolean isCharMatchable(char fuzzedChar) {
		boolean possibleMatcher = false;
		// we set all matchers to mismatch (to overrule possible interdependencies)
		for(Matcher curMatcher : matchers)
			curMatcher.matches = false;
		
		for(Matcher curMatcher : matchers) {
			curMatcher.reset();
			curMatcher.match(fuzzedChar);
			possibleMatcher = possibleMatcher || curMatcher.match('\0');
			curMatcher.matches = false;
		}
		return possibleMatcher;
	}
	private char getRandomPrintable(Random random) {
		return (char)(random.nextInt('~' - ' ')+' ');
	}
	/**
	 * Generates a single random test case.
	 * @param random the random number generator
	 * @return a random program, that the lexer should be able to parse
	 */
	public String fuzzify(Random random) {
		// in 1/10 of cases we emit a random whitespace
		if(random.nextInt(10)==0) {
			StringBuilder sb = new StringBuilder();
			int len = random.nextInt(6);
			for(int i=0; i<len; i++)
				switch(random.nextInt(3)) {
					case 0: sb.append(' '); break;
					case 1: sb.append('\n'); break;
					case 2: sb.append('\t'); break;
				}
			return sb.toString();
		}
		
		// in 1/100 of cases we emit a random comment with 1/3 of a second one
		if(random.nextInt(100)==0) {
			StringBuilder sb = new StringBuilder();
			boolean commCont = true;
			while(commCont) {
				sb.append('#');
				int len = random.nextInt(50);
				for(int i=0; i<len; i++)
					sb.append(getRandomPrintable(random));
				if(random.nextInt(2)==0)
					sb.append('\n');
				else
					commCont = false;
			}
			return sb.toString();
		}
		
		// otherwise we generate a random symbol
		Matcher matcher = matchers[random.nextInt(matchers.length)];
		
		if(matcher instanceof KeywordMatcher) {
			KeywordMatcher kMatch = ((KeywordMatcher)matcher);

			char validTerm;
			do {
				do
					validTerm = getRandomPrintable(random);
				while(!kMatch.validEnding(validTerm));
			} while(!isCharMatchable(validTerm));
			return kMatch.definition+validTerm;
		}
		if(matcher instanceof OperatorMatcher) {
			return ((OperatorMatcher)matcher).definition;
		}
		if(matcher instanceof NondelimitedRuleMatcher) {
			int len = random.nextInt(10);
			// we reset our picked matcher and generate random characters for so
			// long until we get a valid random string of specified length
			StringBuilder sb = new StringBuilder();
			
			char randChar;
			int matchLen = 0;
			do {
				matcher.reset();
				randChar = getRandomPrintable(random);
			} while(!((NondelimitedRuleMatcher)matcher).validStart(randChar));
			sb.append(randChar);
			
			while(matchLen < len) {
				randChar = getRandomPrintable(random);
				if(((NondelimitedRuleMatcher)matcher).validContent(randChar)) {
					sb.append(randChar);
					matchLen++;
				}
				else
					matcher.reset();
			}
			
			do {
				matcher.reset();
				randChar = getRandomPrintable(random);
			} while(!matcher.validEnding(randChar) || !isCharMatchable(randChar));
			sb.append(randChar);
			
			//if(matcher instanceof IntegerMatcher)
			//	sb.append(' ');
			
			return sb.toString();
		}
		if(matcher instanceof CharacterMatcher) {
			char randChar;
			do
				randChar = getRandomPrintable(random);
			while(randChar=='\'');
			return "\'"+randChar+"\'";
		}
		return " ";
	}
	public static void main(String[] args) throws IOException {
		LexAn la = new LexAn(true);
		Random r = new Random(3199638);
		int len = 0;
		
		BufferedWriter out = new BufferedWriter(new FileWriter("./data/fuzzer_out.prev"));
		String fuzz;
		while(len < 1024) {
			fuzz = la.fuzzify(r);
			len += fuzz.length();
			out.write(fuzz);
		}
		out.flush();
		out.close();
	}
}